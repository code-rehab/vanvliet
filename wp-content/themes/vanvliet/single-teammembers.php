<?php

get_header(); ?>
<section class="pagewrap">
    <article id="page_content">
        <main>
            <?php get_template_part('partials/content', 'single') ?>
        </main>
        <aside>
            <?php get_template_part('partials/content', 'sidebar-advocaat'); ?>
        </aside>
    </article>
</section>

<?php get_template_part('partials/content', 'latestnews-advocaat'); ?>

<?php get_footer(); ?>
