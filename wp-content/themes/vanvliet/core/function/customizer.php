<?php

function own_customize_register( $wp_customize ) {

    // Sections
    $wp_customize->add_section( 'contact_information' , array(
        'title'      => __( 'Contactinformatie', 'coderehab' ),
        'priority'   => 30,
    ) );

    // Setting and Controller (mind the order, setting first)
    $wp_customize->add_setting( 'email_adres' , array(
        'default'     => 'info@email.nl',
        'type'        => 'option',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control( 'email_adres', array(
        'label'        => __( 'E-mailadres', 'coderehab' ),
        'section'    => 'contact_information',
        'type'        => 'text',
    ) );

    // Setting and Controller (mind the order, setting first)
    $wp_customize->add_setting( 'telephone' , array(
        'default'     => '076 - 520 41 90',
        'type'        => 'option',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control( 'telephone', array(
        'label'        => __( 'Telefoonnummer', 'coderehab' ),
        'section'    => 'contact_information',
        'type'        => 'text',
    ) );

    // Setting and Controller (mind the order, setting first)
    $wp_customize->add_setting( 'fax' , array(
        'default'     => '076 - 522 18 82',
        'type'        => 'option',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control( 'fax', array(
        'label'        => __( 'Faxnnummer', 'coderehab' ),
        'section'    => 'contact_information',
        'type'        => 'text',
    ) );

      // Setting and Controller (mind the order, setting first)
    $wp_customize->add_setting( 'adres' , array(
        'default'     => 'Sophiastraat 40',
        'type'        => 'option',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( 'adres', array(
        'label'        => __( 'Adres', 'coderehab' ),
        'section'    => 'contact_information',
        'type'        => 'text',
    ) );

          // Setting and Controller (mind the order, setting first)
    $wp_customize->add_setting( 'postcode' , array(
        'default'     => '7504 PB Enschede',
        'type'        => 'option',
        'transport'   => 'refresh',
    ) );

    $wp_customize->add_control( 'postcode', array(
        'label'        => __( 'Postcode en plaats', 'coderehab' ),
        'section'    => 'contact_information',
        'type'        => 'text',
    ) );

}
add_action( 'customize_register', 'own_customize_register' );
?>
