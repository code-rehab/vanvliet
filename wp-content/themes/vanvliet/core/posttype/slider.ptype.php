<?php

add_action( 'init', 'create_post_type_slides');
function create_post_type_slides() {

    $singular = 'Slide';
    $plural = 'Slides';

    register_post_type(
        'slides',
        array(
            'labels' => array(
                'name'               => __( $plural, 'coderehab-base' ),
                'singular_name'      => __( $singular, 'coderehab-base' ),
                'all_items'          => __( 'Alle ' . $plural, 'coderehab-base' ),
                'add_new'            => __( 'Nieuwe ' . $singular . ' toevoegen', 'coderehab-base' ),
                'add_new_item'       => __( 'Nieuwe ' . $singular . ' toevoegen', 'coderehab-base' ),
                'edit'               => __( 'Aanpassen', 'coderehab-base' ),
                'edit_item'          => __( $singular . ' bewerken', 'coderehab-base' ),
                'new_item'           => __( 'Nieuwe ' . $singular, 'coderehab-base' ),
                'view'               => __( 'Bekijk ' . $singular, 'coderehab-base' ),
                'view_item'          => __( 'Bekijk ' . $singular, 'coderehab-base' ),
                'search_items'       => __( 'Zoek ' . $plural, 'coderehab-base' ),
                'not_found'          => __( 'Geen ' . $plural. ' gevonden', 'coderehab-base' ),
                'not_found_in_trash' => __( 'Geen ' . $plural. ' in de prullenbak gevonden', 'coderehab-base' ),
                'parent'             => __( 'Hoofd ' . $singular, 'coderehab-base' )
            ),
            'public' => true,
            'has_archive' => true,

            'menu_position' => 5, // Onder berichten plaatsen
            'menu_icon'           => 'dashicons-format-gallery',

            'rewrite' => array(
                'slug' => 'slides',
                'with_front' => false
            ),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'excerpt',
                //              'page-attributes'
            ),
        )
    );
    flush_rewrite_rules();
}

function slidercategory() {
    register_taxonomy(
        'slider-category',
        'slides',
        array(
            'label' => __( 'Categorie' ),
            'rewrite' => array( 'slug' => 'slider-category' ),
            'hierarchical' => true,
        )
    );
}
add_action( 'init', 'slidercategory' );

?>
