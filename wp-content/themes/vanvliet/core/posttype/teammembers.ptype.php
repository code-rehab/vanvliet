<?php

add_action( 'init', 'create_post_type_teammembers');
function create_post_type_teammembers() {

    $singular = 'Teammember';
    $plural = 'Teammembers';

    register_post_type(
        'teammembers',
        array(
            'labels' => array(
                'name'               => __( 'Team', 'coderehab-base' ),
                'singular_name'      => __( $singular, 'coderehab-base' ),
                'all_items'          => __( 'Alle ' . $plural, 'coderehab-base' ),
                'add_new'            => __( 'Nieuwe ' . $singular . ' toevoegen', 'coderehab-base' ),
                'add_new_item'       => __( 'Nieuwe ' . $singular . ' toevoegen', 'coderehab-base' ),
                'edit'               => __( 'Aanpassen', 'coderehab-base' ),
                'edit_item'          => __( $singular . ' bewerken', 'coderehab-base' ),
                'new_item'           => __( 'Nieuwe ' . $singular, 'coderehab-base' ),
                'view'               => __( 'Bekijk ' . $singular, 'coderehab-base' ),
                'view_item'          => __( 'Bekijk ' . $singular, 'coderehab-base' ),
                'search_items'       => __( 'Zoek ' . $plural, 'coderehab-base' ),
                'not_found'          => __( 'Geen ' . $plural. ' gevonden', 'coderehab-base' ),
                'not_found_in_trash' => __( 'Geen ' . $plural. ' in de prullenbak gevonden', 'coderehab-base' ),
                'parent'             => __( 'Hoofd ' . $singular, 'coderehab-base' )
            ),
            'public' => true,
            'has_archive' => true,

            'menu_position' => 5, // Onder berichten plaatsen
            'menu_icon'           => 'dashicons-groups',

            'rewrite' => array(
                'slug' => 'team',
                'with_front' => false
            ),
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'excerpt',
                //              'page-attributes'
            ),
        )
    );
    flush_rewrite_rules();
}
?>
