<div class="blog_select">
    <p>
        <select name='blog' id='blog'>

            <?php
            //            $taxonomy = "blog-category";
            $tax_terms = get_categories();

            foreach ($tax_terms as $tax_term) {

                //                var_dump($tax_term);

                if ($tax_term->slug == $blog){
                    echo '<option value="' . $tax_term->slug . '"selected>' . $tax_term->name . '</option>';
                } else {
                    echo '<option value="' . $tax_term->slug . '">' . $tax_term->name . '</option>';
                }
            };

            if ($blog == "all"){
                echo '<option value="all" selected>Alle Categorieen</option>';
            } else {
                echo '<option value="all">Alle Categorieën</option>';
            }

            ?>

        </select>
    </p>

    <p>
        <input type="checkbox" id='noblog' name="noblog" <?php checked( $noblog, 'on' ); ?> /> Geen blog op deze pagina tonen
    </p>

</div>
