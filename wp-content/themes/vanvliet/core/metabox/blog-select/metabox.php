<?php

add_action( 'add_meta_boxes', 'blog_meta_box_add' );
add_action( 'save_post', 'blog_meta_box_save' );

function blog_meta_box_add()
{
    add_meta_box( 'blog_select', 'Blog', 'blog_menu_box', 'page', 'normal', 'high' );
}

function blog_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $blog = isset( $values['blog'] ) ? esc_attr( $values['blog'][0] ) : "";
    $noblog = isset( $values['noblog'] ) ? esc_attr( $values['noblog'][0] ) : "";

    include 'view.php';
}

function blog_meta_box_save( $post_id )
{
//  var_dump($_POST);

      // Defines

    $blog = isset($_POST['blog']) ? $_POST['blog'] : '' ;
    $noblog = isset($_POST['noblog']) ? $_POST['noblog'] : '' ;

/*    if( isset($_POST['blog']) ) {
        $blog = $_POST['blog'];
    }

    if( isset($_POST['noblog']) ) {
        $noblog = $_POST['noblog'];
    }*/

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['blog'] ) ) {
        update_post_meta( $post_id, 'blog', $blog);
    }

    if( isset( $_POST['noblog'] ) ) {
        update_post_meta( $post_id, 'noblog', $noblog);
    } else {
        delete_post_meta( $post_id, 'noblog', $noblog);
    }
}

?>
