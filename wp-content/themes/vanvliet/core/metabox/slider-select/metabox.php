<?php

add_action( 'add_meta_boxes', 'slider_meta_box_add' );
add_action( 'save_post', 'slider_meta_box_save' );

function slider_meta_box_add()
{
    add_meta_box( 'slider_select', 'Slider', 'slider_menu_box', 'page', 'normal', 'high' );
}

function slider_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $slider = isset( $values['slider'] ) ? esc_attr( $values['slider'][0] ) : "";
    $noslider = isset( $values['noslider'] ) ? esc_attr( $values['noslider'][0] ) : "";

    include 'view.php';
}

function slider_meta_box_save( $post_id )
{

      $slider = isset($_POST['slider']) ? $_POST['slider'] : '' ;
    $noslider = isset($_POST['noslider']) ? $_POST['noslider'] : '' ;

/*    // Defines
    if( isset($_POST['slider']) ) {
        $slider = $_POST['slider'];
    }

    if( isset($_POST['noslider']) ) {
        $noslider = $_POST['noslider'];
    }*/


    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['slider'] ) ) {
        update_post_meta( $post_id, 'slider', $slider);
    }

    if( isset( $_POST['noslider'] ) ) {
        update_post_meta( $post_id, 'noslider', $noslider);
    } else {
        delete_post_meta( $post_id, 'noslider', $noslider);
    }
}

?>
