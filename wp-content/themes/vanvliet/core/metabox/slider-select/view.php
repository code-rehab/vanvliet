<div class="slider_select">
    <p>
        <select name='slider' id='slider'>

            <?php
            $taxonomy = "slider-category";
            $tax_terms = get_terms($taxonomy);

            foreach ($tax_terms as $tax_term) {

//                var_dump($tax_term);

                if ($tax_term->slug == $slider){
                    echo '<option value="' . $tax_term->slug . '"selected>' . $tax_term->name . '</option>';
                } else {
                    echo '<option value="' . $tax_term->slug . '">' . $tax_term->name . '</option>';
                }
            } ?>

        </select>
    <p>
        <input type="checkbox" id='noslider' name="noslider" <?php checked( $noslider, 'on' ); ?> />  Geen slider op deze pagina tonen
    </p>
    </p>

</div>
