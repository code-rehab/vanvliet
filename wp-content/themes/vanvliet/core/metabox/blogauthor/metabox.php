<?php

add_action( 'add_meta_boxes', 'blogauthor_meta_box_add' );
add_action( 'save_post', 'blogauthor_meta_box_save' );

function blogauthor_meta_box_add()
{
    add_meta_box( 'blogauthor_select', 'Bericht auteur', 'blogauthor_menu_box', 'teammembers', 'normal', 'high' );
}

function blogauthor_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $blogauthor = isset( $values['blogauthor'] ) ? esc_attr( $values['blogauthor'][0] ) : "";

    include 'view.php';
}

function blogauthor_meta_box_save( $post_id )
{

    // Defines
    if( isset($_POST['blogauthor']) ) {
        $blogauthor = $_POST['blogauthor'];
    }


    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['blogauthor'] ) ) {
        update_post_meta( $post_id, 'blogauthor', $blogauthor);
    }
}

?>
