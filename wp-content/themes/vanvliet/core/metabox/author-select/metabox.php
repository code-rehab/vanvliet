<?php

add_action( 'add_meta_boxes', 'author_meta_box_add' );
add_action( 'save_post', 'author_meta_box_save' );

function author_meta_box_add()
{
    add_meta_box( 'author_select', 'Bericht auteur', 'author_menu_box', 'post', 'normal', 'high' );
}

function author_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $author = isset( $values['author'] ) ? esc_attr( $values['author'][0] ) : "";

    include 'view.php';
}

function author_meta_box_save( $post_id )
{

    // Defines
    if( isset($_POST['author']) ) {
        $author = $_POST['author'];
    }


    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['author'] ) ) {
        update_post_meta( $post_id, 'author', $author);
    }
}

?>
