<?php

add_action( 'add_meta_boxes', 'contactinformatie_box_add' );
add_action( 'save_post', 'contactinformatie_box_save' );

function contactinformatie_box_add()
{
    add_meta_box( 'date_select', 'Contactinformatie', 'contactinformatie_menu_box', 'teammembers', 'normal', 'high' );
}

function contactinformatie_menu_box( $post )
{
    $values = get_post_custom( $post->ID );
    $emailadres = isset( $values['emailadres'] ) ? esc_attr( $values['emailadres'][0] ) : "";
    $telefoonnummer = isset( $values['telefoonnummer'] ) ? esc_attr( $values['telefoonnummer'][0] ) : "";
    $linkedin = isset( $values['linkedin'] ) ? esc_attr( $values['linkedin'][0] ) : "";
    $voornaam = isset( $values['voornaam'] ) ? esc_attr( $values['voornaam'][0] ) : "";

    include 'view.php';
}

function contactinformatie_box_save( $post_id )
{

    // Defines
    if( isset($_POST['emailadres']) ) {
        $emailadres = $_POST['emailadres'];
    }

    if( isset($_POST['telefoonnummer']) ) {
        $telefoonnummer = $_POST['telefoonnummer'];
    }

    if( isset($_POST['linkedin']) ) {
        $linkedin = $_POST['linkedin'];
    }

    if( isset($_POST['voornaam']) ) {
        $voornaam = $_POST['voornaam'];
    }

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post', $post_id ) ) return;
    //var_dump( $_POST ); die;

    if( isset( $_POST['emailadres'] ) ) {
        update_post_meta( $post_id, 'emailadres', $emailadres);
    }

    if( isset( $_POST['telefoonnummer'] ) ) {
        update_post_meta( $post_id, 'telefoonnummer', $telefoonnummer);
    }

    if( isset( $_POST['linkedin'] ) ) {
        update_post_meta( $post_id, 'linkedin', $linkedin);
    }

    if( isset( $_POST['voornaam'] ) ) {
        update_post_meta( $post_id, 'voornaam', $voornaam);
    }

}

?>
