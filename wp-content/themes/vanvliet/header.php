<!doctype html>

<html>
    <head>
        <meta charset="utf-8">
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="author" content="Code.Rehab" />
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
        <!--<meta name="theme-color" content="#f57823" >-->

        <title><?php wp_title(); ?></title>

        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/site.css"  />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/flickity.css"  />
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

        <?php wp_head(); ?>
    </head>

    <body <?php body_class() ?> >

        <header id="main">
            <section class="topbar"></section>
            <section class="pagewrap">
                <section id="logo">
                    <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png"/></a>
                </section>
                <a class="toggle-menu-button"></a>

                <nav>

                    <?php if ( is_active_sidebar( 'header' ) ) : ?>
                    <section class="header-widgets">
                        <?php dynamic_sidebar( 'header' ); ?>
                    </section>
                    <?php endif; ?>

                    <?php wp_nav_menu( array('theme_location' => 'main-menu') ); ?>
                    <span class="close">x</span>
                </nav>

            </section>
            <section class="bottombar dark"></section>
        </header>

        <?php get_template_part('partials/content', 'slider'); ?>

        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<section class="pagewrap"><p id="breadcrumbs">','</p></section>');
        }
        ?>
