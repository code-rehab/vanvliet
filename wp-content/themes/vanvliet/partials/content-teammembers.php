<section class="teammembers">
  <h2>Ons team</h2>
  <?php
  $args = array(
    'post_type' => 'teammembers',
  );

  $loop = new WP_Query( $args );
  while ( $loop->have_posts() ) : $loop->the_post();?>

  <?php
  $posttype = get_post_type(get_the_ID());
  $permalink_archive = get_post_type_archive_link($posttype);

  $thumb_id = get_post_thumbnail_id(get_the_ID());
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
  $thumb_url = $thumb_url_array[0];
  $availableImage = "has-thumb";
  ?>

  <section class="teammember">
    <?php if (has_post_thumbnail()) {?>
    <a href="<?php echo get_the_permalink(); ?>"><?php echo the_post_thumbnail('thumbnail'); ?></a>
    <?php } else{
  $availableImage = "no-image";
}?>

    <a href="<?php echo get_the_permalink(); ?>"><h3 class="<?php echo $availableImage ?>"><?php echo str_replace(' | ', '<br />', get_the_title()); ?></h3></a>
    <h4><?php echo get_the_subtitle(); ?></h4>

    <p class="info hyphens"><?php echo get_the_excerpt(); ?> <a href="<?php echo get_the_permalink(); ?>">Lees meer</a></p>
  </section>
  <hr>

  <?php
  endwhile;
  wp_reset_query();
  ?>
</section>
