<?php
$noslider = get_post_meta( get_the_ID(), 'noslider', true );
$category = get_post_meta( get_the_ID(), 'slider', true );

//Loop om slides te tonen
$args = array(
    'post_type' => 'slides',
    	'tax_query' => array(
		array(
			'taxonomy' => 'slider-category',
			'field'    => 'slug',
			'terms'    => $category,
		),
	),
);

$loop = new WP_Query( $args );

// Check of de slider wel getoond kan en mag worden
if ( $noslider != 'on' && $loop->have_posts()) : ?>
<!--if ($loop->have_posts()) : ?>-->

<section class="headerslider">
    <ul>
        <?php while ( $loop->have_posts() ) : $loop->the_post();?>

        <?php
        $thumb_id = get_post_thumbnail_id();
        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'large', true);
        $thumb_url = $thumb_url_array[0];
        ?>

        <li class="slide" style="background-image: url(<?php echo $thumb_url ?>)">
            <!--        <section class="absolute-wrapper"></section>-->
        </li>

        <?php 
    wp_reset_query();
            endwhile; 
        ?>

    </ul>
</section>

<section class="pagewrap slider">
    <section class="headerslider-navigation">
        <section class="navigation">
            <a class="prev" href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/arrow-left.png"></a>
            <a class="next" href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/arrow-right.png"></a>
        </section>
    </section>
</section>
<section class="bottombar dark"></section>

<?php endif; ?>
