<?php
//Loop om slides te tonen
$args = array(
    'post_type' => 'specialiteiten',
);
?>

<section class="specialisaties">
    <section class="about-us">
        <?php echo get_the_content(); ?> <a href="<?php echo site_url() . '/'; ?>">Lees meer</a>
    </section>

    <h2>Rechtsgebieden</h2>
    <?php
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();?>

    <?php
    $posttype = get_post_type(get_the_ID());
    $permalink_archive = get_post_type_archive_link($posttype);

    $thumb_id = get_post_thumbnail_id(get_the_ID());
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
    $thumb_url = $thumb_url_array[0];
    ?>

    <div class="specialisatie">
        <?php if (has_post_thumbnail()) {?>
        <a href="<?php echo get_the_permalink(); ?>"><img src="<?php echo $thumb_url ?>"/></a>
        <?php } else{
    $availableImage = "no-image";
}?>

        <a href="<?php echo get_the_permalink(); ?>"><h3 class="<?php echo $availableImage ?>"><?php echo get_the_title(); ?></h3></a>

        <p class="info"><?php echo get_the_excerpt(); ?> <a href="<?php echo get_the_permalink(); ?>">Lees meer</a></p>
    </div>
    <hr>

    <?php endwhile; ?>
</section>
