<h1>Contact</h1>
<!--
<a href="/contact"><img src="<?php bloginfo('template_url'); ?>/assets/images/bel.png"></a>
<a href="/contact"><img src="<?php bloginfo('template_url'); ?>/assets/images/email.png"></a>
<a href="/contact"><img src="<?php bloginfo('template_url'); ?>/assets/images/linkedin.png"></a>
-->

<?php

$naam = get_post_meta( get_the_ID(), 'voornaam', true );
$telefoon = get_post_meta( get_the_ID(), 'telefoonnummer', true );
$email = get_post_meta( get_the_ID(), 'emailadres', true );
$linkedin = get_post_meta( get_the_ID(), 'linkedin', true );
$template = get_bloginfo('template_url');

if (!empty($telefoon)) {
    echo '<p><a class="button team tel" href="tel:' . $telefoon . '" data-tel="' . $telefoon . '"><span><i class="fa fa-mobile" aria-hidden="true"></i> Bel ' . $naam . '</span></a></p>';
}

if (!empty($email)) {
    echo '<p><a class="button team" href="mailto:' . $email . '"><span><img src="' . $template . '/assets/images/icons/mail.png">E-mail ' . $naam . '</span></a></p>';
}

if (!empty($linkedin)) {
    echo '<p><a class="button team" href="' . $linkedin . '"><span><img src="' . $template . '/assets/images/icons/linkedin.png">LinkedIn</span></a></p>';
}

?>
