<?php
$category = get_post_meta( get_the_ID(), 'blog', true );

//Loop om slides te tonen
if ($category !== "all"){
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
    'category_name' => $category,
  );
} else {
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 3,
  );
}

$loop = new WP_Query( $args );
//    echo '<pre>';
//    var_dump($loop);
//    echo '</pre>';

// voor title weergeven
$i = 0;
if ($loop->have_posts()){
while ( $loop->have_posts() ) : $loop->the_post();?>

<?php
$posttype = get_post_type(get_the_ID());
$permalink_archive = get_post_type_archive_link($posttype);

$thumb_id = get_post_thumbnail_id(get_the_ID());
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
$thumb_url = $thumb_url_array[0];

  ?>
  <section class="grey">
    <section class="pagewrap">
      <section class="newsitems">
        <?php
        $blog = get_post_meta( get_the_ID(), 'blog', true );

        if( $blog == "all" || $blog == null){
          echo '<h2>Blog</h2>';
        } else {
          echo '<h2>Blog #' . $blog . '</h2>';
        }
        ?>
        <div class="newsitem">
          <div class="content">
            <?php if (has_post_thumbnail()) {?>
              <a href="<?php echo get_the_permalink(); ?>"><img src="<?php echo $thumb_url ?>"/></a>
              <?php } else{
                $availableImage = "no-image";
              }?>

              <a href="<?php echo get_the_permalink(); ?>"><h2><?php echo get_the_title(); ?></h2>
                <?php
                $categories = get_the_category( $post->ID );

                foreach( $categories as $category ) {
                  echo '<span>#' . $category->name . ' </span>';
                }
                ?>
              </a>

              <p class="info"><?php echo get_the_excerpt(); ?> <a href="<?php echo get_the_permalink(); ?>">Lees meer</a></p>
            </div>

            <div class="post-author">
              <?php


              if($i == 0){
                echo '<h2>Auteur</h2>';
              }
              $i++;

              $author = get_post(get_post_meta( get_the_ID(), 'author', true ));
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_post_meta( get_the_ID(), 'author', true )), 'thumbnail', true );
              $thumbnail = $thumb['0'];

              echo '<img class="rounded" src="' . $thumbnail . '"/>';
              echo '<h3><a href="' . get_the_permalink($author->ID) . '">' . $author->post_title . '</a></h3>';
              echo '<p class="info">' . $author->post_excerpt . '<a href="' . get_the_permalink() . '">Lees meer</a></p>';
              ?>
            </div>
          </div>
        <?php endwhile; ?>

        <section class="read-more">
          <a href="<?php if( get_option( 'show_on_front' ) == 'page' ) echo get_permalink( get_option('page_for_posts' ) );
          else echo bloginfo('url');?>" class="button">Meer blogposts</a>
        </section>
      </section>
    </section>
  </section>
  <?php } ?>
