<h1>Contact</h1>
<!-- <a href="/contact"><img src="<?php bloginfo('template_url'); ?>/assets/images/afspraak-maken.png"></a> -->
            <a href="tel:<?php echo get_option("telephone"); ?>" data-tel="<?php echo get_option("telephone"); ?>" class="button contact tel"><i class="fa fa-mobile" aria-hidden="true"></i>Maak een afspraak</a>
<p>
Meer informatie over hoe wij u hierin bij kunnen
staan? Vul dan onderstaand formulier in en we zorgen dat u
de juiste informatie ontvangt.
</p>

<?php echo do_shortcode('[contact-form-7 id="70" title="Contact form 1"]'); ?>
