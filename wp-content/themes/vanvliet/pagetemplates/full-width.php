<?php

/*
Template Name: Volle Paginabreedte
*/

get_header(); ?>
<section class="pagewrap">
    <article id="page_content">
        <main>
            <?php get_template_part('partials/content', 'single') ?>
        </main>
    </article>
</section>

<section class="grey">
    <section class="pagewrap">
        <?php get_template_part('partials/content', 'latestnews'); ?>
    </section>
</section>

<?php get_footer(); ?>
