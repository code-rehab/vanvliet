<?php

/*
Template Name: Homepage
*/

get_header();

while ( have_posts() ) : the_post();

?>
<main>
  <section class="pagewrap">
    <section class="specialisaties">
      <section class="about-us">
        <?php echo get_the_content(); ?>
      </section>
    </section>
    <!-- <?php get_template_part('partials/content', 'specials'); ?> -->

    <aside>
      <?php get_template_part('partials/content', 'teammembers'); ?>
    </aside>
  </section>
</main>

<?php
$noblog = get_post_meta( get_the_ID(), 'noblog', true );
if ( $noblog != 'on') : ?>

<?php get_template_part('partials/content', 'latestnews'); ?>

<?php endif; ?>

<?php
endwhile;
get_footer();

?>
