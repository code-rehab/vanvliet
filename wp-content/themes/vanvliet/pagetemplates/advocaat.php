<?php

/*
Template Name: Advocaat contactpagina
*/

get_header(); ?>
<section class="pagewrap">
    <article id="page_content">
        <main>
            <?php get_template_part('partials/content', 'single') ?>
        </main>
        <aside>
            <?php get_template_part('partials/content', 'sidebar-advocaat'); ?>
        </aside>
    </article>
</section>

<section class="grey">
    <section class="pagewrap">
        <?php get_template_part('partials/content', 'latestnews-advocaat'); ?>
    </section>
</section>

<?php get_footer(); ?>
