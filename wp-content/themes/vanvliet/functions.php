<?php

//display errors
// error_reporting(E_ALL);
ini_set('display_errors', 0);

//INCLUDES
//=======================================================================
require_once "core/function/mobile-detect.php";
require_once "core/function/featured-image.php";
require_once "core/function/customizer.php";

require_once "core/posttype/slider.ptype.php";
require_once "core/posttype/specials.ptype.php";
require_once "core/posttype/teammembers.ptype.php";

require_once "core/metabox/author-select/metabox.php";
require_once "core/metabox/slider-select/metabox.php";
require_once "core/metabox/blog-select/metabox.php";
require_once "core/metabox/contactinformatie/metabox.php";
require_once "core/metabox/blogauthor/metabox.php";

//SETTINGS, ACTIONS, FILTERS
//=======================================================================

//settings
add_theme_support(
    'post-thumbnails', array(
        'post',
        'events',
        'page',
        'slides',
        'teammembers',
    )
);

//actions
add_action( 'widgets_init', 'register_default_sidebars' );
add_action( 'after_setup_theme', 'register_default_menus' );
add_action( 'admin_init', 'set_custom_editor_stylesheet' );

//filters
add_filter('mce_buttons_2', 'add_editor_styleselect_btn');
add_filter( 'tiny_mce_before_init', 'setup_custom_editor_settings' );
add_filter('wp_mail_from', 'set_mail_from');
add_filter('wp_mail_from_name', 'set_mail_from_name');


// functions
//=======================================================================

//action: widgets_init
function register_default_sidebars() {
    register_sidebar( array(
        'name'          => __( 'Sidebar', 'default-sidebar' ),
        'id'            => 'page-sidebar',
        'before_widget' => '<div class="sidebar-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

        register_sidebar( array(
        'name'          => __( 'Header', 'default-sidebar' ),
        'id'            => 'header',
        'before_widget' => '<div class="header-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));
}

//action: after_setup_theme
//register menus
function register_default_menus() {
    register_nav_menu( 'main-menu', 'Main-menu' );
    register_nav_menu( 'footer-menu', 'footer-menu' );
    register_nav_menu( 'specialiteiten', 'Specialiteiten' );
    register_nav_menu( 'advocaten', 'Advocaten' );
}


// filter: tiny_mce_before_init
function setEditorColors( $init ) {

}

// filter: wp_mail_from
function set_mail_from($old) {
    return get_option('admin_email');
}

// filter: wp_mail_from_name
function set_mail_from_name($old) {
    return get_option('blogname');
}

//filter:mce_buttons_2
function add_editor_styleselect_btn( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}

function set_custom_editor_stylesheet() {
    add_editor_style( 'assets/css/wp-editor-style.css' );
}

//filter:tiny_mce_before_init
function setup_custom_editor_settings( $init_array ) {

    // Define the style_formats array
    $style_formats = array(
        array(
            'title' => 'Button',
            'inline' => 'span',
            'classes' => 'button',
            'wrapper' => false,

        ),

        array(
            'title' => 'Button-full-width',
            'inline' => 'span',
            'classes' => 'button full-width',
            'wrapper' => false,
        ),
    );

    //change default colors
    $default_colours = '[
	    "000000", "Black",
	    "ffffff", "White",
	    "ababab", "Gray",
	    "FA950F", "Orange1",
	    "DF6421", "Orange2",
	    "096af3", "Blue1",
	    "389cff", "Blue2",
	]';


    $init_array['textcolor_map'] = $default_colours;
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}

function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
