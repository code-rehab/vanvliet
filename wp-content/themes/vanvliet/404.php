<?php get_header(); ?>
<section class="pagewrap">
    <article id="page_content">
        <main>
            <h1>404 Pagina niet gevonden</h1>
            <a href="<?php echo home_url(); ?>"><h3>Keer terug naar de homepage</h3></a>
        </main>
    </article>
</section>
<?php get_footer(); ?>
