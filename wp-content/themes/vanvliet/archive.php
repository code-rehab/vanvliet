<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<main>
    <article>
        <?php get_template_part('partials/content', 'listitem'); ?>
    </article>
</main>
<?php endwhile; ?>

<?php get_footer(); ?>
