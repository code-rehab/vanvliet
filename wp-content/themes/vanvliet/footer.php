<?php wp_footer(); ?>

<footer id="main">
    <section class="pagewrap">
        <section class="menu advocaten">
            <h2>Ons team</h2>
            <?php wp_nav_menu( array('theme_location' => 'advocaten') ); ?>
        </section>

        <section class="menu">
            <h2>Rechtsgebieden</h2>
            <?php wp_nav_menu( array('theme_location' => 'specialiteiten') ); ?>
        </section>

        <section class="contact">
            <h2>Contact</h2>
            <span>T.</span> <a href="tel:<?php echo get_option("telephone"); ?>"><?php echo get_option("telephone"); ?></a><br/>
            <span>F. <?php echo get_option("fax"); ?></span><br/><br/>

            <span><?php echo get_option("adres"); ?></span><br/>
            <span><?php echo get_option("postcode"); ?></span><br/><br/>

            <a href="mailto:<?php echo get_option("email_adres"); ?>"><?php echo get_option("email_adres"); ?></a><br/><br/>
            <a target="_blank" href="https://www.linkedin.com/company/17893794"><i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i></a>
        </section>

        <section class="logo">
            <a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/footer-logo.png"></a>
            <a href="tel:<?php echo get_option("telephone"); ?>" class="button contact tel" data-tel="<?php echo get_option("telephone"); ?>"><i class="fa fa-mobile" aria-hidden="true"></i>Maak een afspraak</a>
        </section>
    </section>
</footer>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/wp-core-scripts.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/site.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/external/flickity/jquery.flickity.min.js"></script>

</body>
</html>
