<?php get_header(); ?>
<section class="pagewrap">
    <article id="page_content">
        <main>
            <?php get_template_part('partials/content', 'single') ?>
        </main>
    </article>
</section>



<?php get_footer(); ?>
