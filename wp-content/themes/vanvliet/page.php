<?php get_header(); ?>
<section class="pagewrap">
  <article id="page_content">
    <main>
      <?php get_template_part('partials/content', 'single') ?>
    </main>
    <aside>
      <?php get_template_part('partials/content', 'sidebar'); ?>
    </aside>
  </article>
</section>

<?php
$noblog = get_post_meta(get_the_ID(), "noblog", true);

if ( $noblog != 'on') : ?>

<section class="grey">
  <section class="pagewrap">
    <?php get_template_part('partials/content', 'latestnews'); ?>
  </section>
</section>

<?php
endif;
get_footer();
?>
