initMenu();
sliderResize();

$( document ).ready(function() {
  homepageSlider();
  previousSlide();
  nextSlide();

  if ($(window).width() > 768) {
    var value;
    $('a[href^="tel:"]').hover(function(){
      var tel = $(this).data("tel");
      value = $(this).html();
      $(this).html('<span class="phone"><i class="fa fa-mobile" aria-hidden="true"></i>' + tel + '</span>');
    }, function(){
      $(this).html(value);
    });

    $('a[href^="tel:"]').on('click', function(e){
      e.preventDefault();
      var tel = $(this).data("tel");
    });

    $(".menu-item-has-children").hover(
      function(){
        $(".bottombar.dark").first().addClass('menu-hover');
      },
      function () {
        $(".bottombar.dark").first().removeClass('menu-hover');
      }
    );
  };

});


// Sliders
var homepageSlider;
function homepageSlider(){
  // Sticky Slider
  headerslider = new Flickity("section.headerslider ul", {
    prevNextButtons: false,
    pageDots: false,
    wrapAround: true,
    autoPlay: 4000,
    initialIndex: 0,
    cellSelector: '.slide',
    setGallerySize: false,
  });
}

function sliderResize(){
  $( window ).resize(function() {
    //        slidernavigationPosition();

    headerslider.resize();
    headerslider.reposition();
  });
}

//Flickity.prototype.hasDragStarted = function( moveVector ) {
//    // start dragging after pointer has moved 30 pixels in either direction
//    return !this.isTouchScrolling && Math.abs( moveVector.x ) > 30;
//};

function previousSlide(){
  $('section.headerslider-navigation a.prev').on( 'click', function(e) {
    e.preventDefault();
    headerslider.previous();
  });
}

function nextSlide(){
  $('section.headerslider-navigation a.next').on( 'click', function(e) {
    e.preventDefault();
    headerslider.next();
  });
}
