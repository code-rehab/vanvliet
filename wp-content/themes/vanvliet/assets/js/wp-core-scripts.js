if($){
    var onWindowResize = function(args){
        if($(window).innerWidth() < 840){
            $(args.menu_toggle_button).show();
        }else{
            $(args.menu_toggle_button).hide();
            $(args.menu_selector).removeClass('visible')
        }
    }

    function initMenu(args){
        args = args || {
            menu_selector: "header#main nav",
            menu_toggle_button: ".toggle-menu-button",
        };

        $(window).resize(function(e){
            onWindowResize(args);
        })
        onWindowResize(args);

        $(args.menu_toggle_button).on("click", function(e){
            $(args.menu_selector).toggleClass('visible')
            $('body').toggleClass('open-menu')
        });

        $(args.menu_selector).find('.close').on("click", function(e){
            if($(args.menu_selector + ' .sub-menu.visible').length){
                $(args.menu_selector + ' .sub-menu.visible').removeClass('visible')
                $('body').removeClass('open-menu')
            }else{
                $(args.menu_selector).removeClass('visible')
                $('body').removeClass('open-menu')
            }

        });

        $(args.menu_selector).find('.menu-item-has-children > a').on("click", function(e){
            e.preventDefault();
            visibleMenu = $(args.menu_selector).find('.sub-menu.visible');
            menuToShow = $(e.currentTarget).closest('li').find('.sub-menu').first();

            if(visibleMenu.length && !menuToShow.hasClass("visible")){
                visibleMenu.removeClass('visible')

                setTimeout(function(){
                    menuToShow.addClass('visible')
                }, 200)

            }else{
                menuToShow.toggleClass('visible')
            }
        });
    }
}

$(document).mouseup(function(e){
    var container = $("header#main nav .sub-menu.visible");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
//                container.hide();
        container.removeClass('visible')
    }
});
