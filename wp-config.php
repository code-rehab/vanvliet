<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vanvliet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YY+?0[;UMdhIDILcpG=;z,UtwJ//3J&@-SiM;sy87h$C,tYLt[Lwb/1K)j125:(5');
define('SECURE_AUTH_KEY',  'j=qvWC<*tO9/~0T`@8D?+Shm+1HQEE=Oze|/qtpyj5@xz;nFmERW&rv6uh[5!^$#');
define('LOGGED_IN_KEY',    'SISo_ahX_Vv@A1sE(p)I|2it/nHBhiyw<@tXG-I*5/FUY7hj^r!?&^hx:e{g1Mb~');
define('NONCE_KEY',        '2CO_05`ZAid`tN[E6G..RyZfCYk^k5i C<_C(3j][4be3veIs^;;DNVavu*D`8[x');
define('AUTH_SALT',        ';Yp<7J`g&L$:>  &8?(#BD*n}M0/HHG2{i#s5?,tCfum;#L{^[wq/dvZc}Db1! %');
define('SECURE_AUTH_SALT', '+7%y}o%[2@;roGO-T;xA1l0wQdW;C:Kop}El;(Q(t5PviU:/N$h1H0!$jH0ZCRcv');
define('LOGGED_IN_SALT',   'cC? U2Hu/K2XSM(Xf@%0|yX1~dR@}MH8hZq Z?5Sy6;c~Rb&qQ)wKF-x,R s_j E');
define('NONCE_SALT',       '09C$a#-|fym$hZ}0)s%UgN]pS }3!GBArzna<rv5,JI;]=$$&F!H^IgYnl)FKTqY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
